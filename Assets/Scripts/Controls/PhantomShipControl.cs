using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhantomShipControl : MonoBehaviour
{
    const int MAX_FPS = 60;

    public Transform Leader;
    public float LagSeconds = 0.5f;

    Vector3[] PositionBuffer;
    Quaternion[] QuaternionBuffer;
    float[] TimeBuffer;
    int OldestIndex;
    int NewestIndex;

    // Use this for initialization
    void Start()
    {
        int bufferLength = Mathf.CeilToInt(LagSeconds * MAX_FPS);
        PositionBuffer = new Vector3[bufferLength];
        QuaternionBuffer = new Quaternion[bufferLength];
        TimeBuffer = new float[bufferLength];

        PositionBuffer[0] = PositionBuffer[1] = Leader.position;
        QuaternionBuffer[0] = QuaternionBuffer[1] = Leader.rotation;
        TimeBuffer[0] = TimeBuffer[1] = Time.time;


        OldestIndex = 0;
        NewestIndex = 1;
    }


    void LateUpdate()
    {

        if (!Leader) Destroy(gameObject);
        else
        {
            // Insert newest position into our cache.
            // If the cache is full, overwrite the latest sample.
            int newIndex = (NewestIndex + 1) % PositionBuffer.Length;
            if (newIndex != OldestIndex)
                NewestIndex = newIndex;

            PositionBuffer[NewestIndex] = Leader.position;
            QuaternionBuffer[NewestIndex] = Leader.rotation;
            TimeBuffer[NewestIndex] = Time.time;

            // Skip ahead in the buffer to the segment containing our target time.
            float TargetTime = Time.time - LagSeconds;
            int NextIndex;
            while (TimeBuffer[NextIndex = (OldestIndex + 1) % TimeBuffer.Length] < TargetTime)
                OldestIndex = NextIndex;

            // Interpolate between the two samples on either side of our target time.
            float span = TimeBuffer[NextIndex] - TimeBuffer[OldestIndex];
            float progress = 0f;
            if (span > 0f)
            {
                progress = (TargetTime - TimeBuffer[OldestIndex]) / span;
            }

            transform.position = Vector3.Lerp(PositionBuffer[OldestIndex], PositionBuffer[NextIndex], progress);
            transform.rotation = Quaternion.Lerp(QuaternionBuffer[OldestIndex], QuaternionBuffer[NextIndex], progress);
        }
    }

}

