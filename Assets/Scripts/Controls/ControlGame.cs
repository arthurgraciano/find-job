﻿using Gaminho;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class ControlGame : MonoBehaviour {
    public ScenarioLimits ScenarioLimit;
    public Level[] Levels;
    public Image Background;
    private SaveLevel SaveLevel;
    [Header("UI")]
    public Text TextStart;
    public Text TextPoints;
    public Transform BarLife;
    public Text TextPause;
    
    // Use this for initialization
    void Start () {
        Statics.EnemiesDead = 0;
        SetLevel();
        Background.sprite = Levels[Statics.CurrentLevel].Background;
        TextStart.text = "Stage " + (Statics.CurrentLevel + 1);
        GetComponent<AudioSource>().PlayOneShot(Levels[Statics.CurrentLevel].AudioLvl);
        
    }



    private void Update()
    {
        TextPoints.text = Statics.Points.ToString();
        BarLife.localScale = new Vector3(Statics.Life / 10f, 1, 1);

        #region
        if (Input.GetKeyDown(KeyCode.P))
        {
            Statics.isPaused = !Statics.isPaused;
            PauseGame();
        }
        #endregion
    }

    public void LevelPassed()
    {
        
        Clear();
        Statics.CurrentLevel++;
        SaveLevel = new SaveLevel();
        SaveProgress(SaveLevel);
        Statics.Points += 1000 * Statics.CurrentLevel;
        if (Statics.CurrentLevel < 3)
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_LEVELUP) as GameObject);
        }
        else
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_CONGRATULATION) as GameObject);
        }
    }
    //Oops, when you lose (: Starts from Zero
    public void GameOver()
    {
        BarLife.localScale = new Vector3(0, 1, 1);
        Clear();
        Destroy(Statics.Player.gameObject);
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_GAMEOVER) as GameObject);
    }
    private void Clear()
    {
        GetComponent<AudioSource>().Stop();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            Destroy(ini);
        }
    }

    private void SaveProgress(SaveLevel save)
    {
        
        save.LevelReached = Statics.CurrentLevel;
        save.EnemiesDead = Statics.EnemiesDead;
        save.Points = Statics.Points;
        BinaryFormatter bf = new BinaryFormatter();
        string path = Application.persistentDataPath;
        FileStream file = File.Create(path + "/savegame.save");

        bf.Serialize(file, save);
        
        file.Close();
        Debug.Log("Level Saved");
    }

    public SaveLevel LoadProgress()
    {
        string path = Application.persistentDataPath;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file;

        if (File.Exists(path + "/savegame.save"))
        {
            file = File.Open(path + "/savegame.save", FileMode.Open);
            SaveLevel Load = (SaveLevel)bf.Deserialize(file);
            file.Close();

            return Load;
        }

        return null;
    }
    // Save Level after player LevelUp
    private void SetLevel()
    {
        SaveLevel = LoadProgress();
        if (Statics.Continue && SaveLevel != null)
        {
            Statics.CurrentLevel = SaveLevel.LevelReached;
            Statics.EnemiesDead = SaveLevel.EnemiesDead;
            Statics.Points = SaveLevel.Points;
            
        }
    }

    public void PauseGame()
    {
        Debug.Log(Statics.isPaused);
        if (Statics.isPaused)
        {
            Time.timeScale = 0;
            GetComponent<AudioSource>().Pause();
            TextPause.gameObject.SetActive(true);
            return;
        }
        Time.timeScale = 1;
        TextPause.gameObject.SetActive(false);
        GetComponent<AudioSource>().UnPause();
    }

}
