using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;

public class DodgeControl : MonoBehaviour
{
    private float Thurst = 10f;
    private float Angle;
    private float CircleRadius = 500f;



    void LateUpdate()
    {
        StartCoroutine(DodgeMech());
    }

    private IEnumerator DodgeMech()
    {

        Collider2D hit2D = Physics2D.OverlapCircle(transform.position, CircleRadius);
        Angle += Thurst * Time.deltaTime;
        var Offset = new Vector2(Mathf.Sin(Angle), Mathf.Cos(Angle)) * CircleRadius;
        var destination = new Vector2(transform.position.x + Offset.x,
            transform.position.y + Offset.y);



        if (hit2D.transform.tag == "Shot")
        {
            
            transform.position = Vector2.Lerp(transform.position,
                destination, 1);
            yield return new WaitForSeconds(0.1f);
        }
        yield return null;
    }

}
