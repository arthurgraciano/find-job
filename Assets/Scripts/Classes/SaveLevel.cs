using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gaminho
{
    [System.Serializable]
    public class SaveLevel
    {
        public int LevelReached;
        public int EnemiesDead;
        public int Points;

    }
}
